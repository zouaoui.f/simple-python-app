from flask import Flask
from environs import Env

app = Flask(__name__)



env = Env()
# Read .env into os.environ
env.read_env()

portnum = env.int("PORT")

@app.route('/')
def hello():
    return 'Well done!'

if __name__ == "__main__":
    app.run( port=portnum)   